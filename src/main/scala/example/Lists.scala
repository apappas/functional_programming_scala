package example

import scala.annotation.tailrec


object Lists {

  /**
    * This method computes the sum of all elements in the list xs. There are
    * multiple techniques that can be used for implementing this method, and
    * you will learn during the class.
    *
    * For this example assignment you can use the following methods in class
    * `List`:
    *
    *  - `xs.isEmpty: Boolean` returns `true` if the list `xs` is empty
    *  - `xs.head: Int` returns the head element of the list `xs`. If the list
    * is empty an exception is thrown
    *  - `xs.tail: List[Int]` returns the tail of the list `xs`, i.e. the the
    * list `xs` without its `head` element
    *
    * ''Hint:'' instead of writing a `for` or `while` loop, think of a recursive
    * solution.
    *
    * @param xs A list of natural numbers
    * @return The sum of all elements in `xs`
    */
  def sum(xs: List[Int]): Int = xs match {
    case h :: t => h + sum(t)
    case _ => 0
  }

  /**
    * This method returns the largest element in a list of integers. If the
    * list `xs` is empty it throws a `java.util.NoSuchElementException`.
    *
    * You can use the same methods of the class `List` as mentioned above.
    *
    * ''Hint:'' Again, think of a recursive solution instead of using looping
    * constructs. You might need to define an auxiliary method.
    *
    * @param xs A list of natural numbers
    * @return The largest element in `xs`
    * @throws java.util.NoSuchElementException if `xs` is an empty list
    */
  /*def max2(xs: List[Int]): Int = xs match {
    case h :: h1 :: t => if (h => h1) max2(h :: t) else max2(h1 :: t)
    case h :: h1 :: _ => if (h => h1) h else h1
    case h :: _ => h
    case _ => throw new NoSuchElementException

  }
*/
  def max(xs: List[Int]): Int = {
    @tailrec def go(maxNum: Int, li: List[Int]): Int = li match {
      case h::t => if(h >= maxNum) go(h, t) else go(maxNum, t)
      case _ => maxNum
    }

    xs match {
      case h::_ => go(0, xs)
      case _ => throw new NoSuchElementException
    }

  }
}
